# Simple website boilerplate with Gulp 4

Boilerplate for simple website or other front-end project made with html, scss and javascript. It's based on @thecodercoder: [front-end boilerplate](https://github.com/thecodercoder/frontend-boilerplate) (github repo). I added image compression and browser-sync for local live preview. It also copy files from source folder to public.

##Requirements

- [Node.js with NPM](https://nodejs.org/en/download/)
- [Gulp 4](https://gulpjs.com/)

##Getting started
1. Clone or download this repo

2. Run `$ npm install`

3. Run gulp with one of command below:

	* `$ gulp` - compile and minify scss to css, concatenate and Uglify JS files, compress 	images, run and stream project files to local server and watches files for changes

	* `$ gulp build` - same as gulp but without running a local server - build project for production

	* `$ gulp images` - copy and compress images to public/img