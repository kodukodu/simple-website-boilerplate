// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
var replace = require('gulp-replace');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

// File paths
const scssPath = 'source/scss/**/*.scss';
const jsPath = 'source/js/**/*.js';
const indexHtmlPath = 'source/**/*.html';
const pagesPath = 'source/pages/**/*.html';

// Sass task: compiles the style.scss file into style.css
function scssTask() {
    return src(scssPath)
        .pipe(sourcemaps.init()) // initialize sourcemaps first
        .pipe(sass()) // compile SCSS to CSS
        .pipe(postcss([autoprefixer(), cssnano()])) // PostCSS plugins
        .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
        .pipe(dest('public/styles')
        ); // put final CSS in dist folder
}

// JS task: concatenates and uglifies JS files to scripts.js
function jsTask() {
    return src([
        jsPath
    ])
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(dest('public/js')
        );
}

// HTML task: copy index html file from source to public directory
function indexHtmlTask() {
    return src([indexHtmlPath])
        .pipe(dest('public/'));
}

// HTML task: copy pages from source/pages to public/pages directory
function pagesTask() {
    return src([pagesPath])
        .pipe(dest('public/pages/'));
}

// Cachebust
var cbString = new Date().getTime();
function cacheBustTask() {
    return src(['source/index.html'])
        .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
        .pipe(dest('public/'));
}

// Copy and optimize images from source directory to public/img
function images() {
    return src("source/img/**/*")
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.jpegtran({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ])
        )
        .pipe(dest("public/img"));
}

// Watch task: watch SCSS and JS files for changes and stream our project to browser with browser-sync
// If any change, run scss and js tasks simultaneously and refresh browser
function watchTask() {
    browserSync.init({
        server: {
            baseDir: "./public"
        },
        notify: false
    });
    watch([scssPath, jsPath, indexHtmlPath, pagesPath],
        series(
            parallel(scssTask, jsTask, indexHtmlTask, pagesTask),
            cacheBustTask
        )
    ).on('change', browserSync.reload);
}

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
    parallel(scssTask, jsTask, indexHtmlTask, pagesTask),
    cacheBustTask,
    watchTask
);

//  Export build task - copies and optimize scss, js, html and images files to public folder
exports.build = series(
    parallel(scssTask, jsTask, indexHtmlTask, pagesTask),
    cacheBustTask
);

// Export images task
exports.images = images;